import numpy as np
import pandas as pd 

# X = np.array(([2,9],[1,5],[3,6]),dtype=float)
# Y = np.array(([92],[86],[89]),dtype=float)

data = pd.DataFrame(data=pd.read_csv('data.csv'))
X = np.array(data.iloc[:,0:-1])
Y = np.array(data.iloc[:,-1])

X = X/np.amax(X,axis=0)
Y = Y/100

class Neural_Network():
    def __init__(self):
        self.inputSize = len(X[0])
        self.hiddenSize = 6
        self.outputSize = 1

        self.W1 = np.random.randn(self.inputSize,self.hiddenSize)
        self.W2 = np.random.randn(self.hiddenSize,self.outputSize)

    def forword(self,X):
        self.z = np.dot(X,self.W1)
        self.z2 = self.sigmoid(self.z)
        self.z3 = np.dot(self.z2,self.W2)
        o = self.sigmoid(self.z3)
        return o

    def sigmoid(self,s):
        return 1/(1+np.exp(-s))

    def sigmoidPrime(self,s):
        return s*(1-s)

    def backward(self,x,y,o):
        self.o_error = Y - o
        self.o_delta = self.o_error * self.sigmoidPrime(o)

        self.z2_error = self.o_delta.dot(self.W2.T)
        self.z2_delta = self.z2_error * self.sigmoidPrime(self.z2)

        self.W1+= X.T.dot(self.z2_delta)
        self.W2+= self.z2.T.dot(self.o_delta)

    def train(self,X,Y):
        o = self.forword(X)
        self.backward(X,Y,o)

NN = Neural_Network()
for i in range(15):
    # print("Input: "+ str(X))
    print("Actual Output: " + str(Y))
    print("Predicted Output: " + str(NN.forword(X)))
    print("Loss :" + str(np.mean(np.square(Y-NN.forword(X)))))
    NN.train(X,Y)

