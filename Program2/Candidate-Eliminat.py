import numpy as np
import pandas as pd

data = pd.DataFrame(data=pd.read_csv('data.csv'))
concepts = np.array(data.iloc[:,0:-1])
target = np.array(data.iloc[:,-1])


def learn(concepts, target):

    spec_index = [ i for i, val in enumerate(target) if val == "Yes" ][0]

    specific_h = concepts[spec_index]

    general_h = [["?" for i in range(len(specific_h))] for i in range(len(specific_h))]

    for i, h in enumerate(concepts):

        if target[i] == "Yes":
            for x in range(len(specific_h)):
                if h[x] != specific_h[x]:
                    specific_h[x] = '?'
                    general_h[x][x] = '?'

        if target[i] == "No":
            for x in range(len(specific_h)):
                if h[x] != specific_h[x]:
                    general_h[x][x] = specific_h[x]
                else:
                    general_h[x][x] = '?'

    general_h = [ a for a in general_h if a != ['?','?','?','?','?','?'] ]

    return specific_h, general_h

s_final, g_final = learn(concepts, target)

print("Final S:", s_final, sep="\n")
print("Final G:", g_final, sep="\n")

