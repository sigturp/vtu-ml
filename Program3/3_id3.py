import csv
import math
import json

def majorClass( data ):
    # get the target column
    targets = [ row[-1] for row in data ]
    print(targets)
    # max occuring value
    major = max( targets, key=targets.count)
    return major


def entropy( attributes, data ):
    freq = {}
    dataEntropy = 0.0
    i = len(attributes) - 2

    for entry in data:
        if entry[i] in freq:
            freq[entry[i]] += 1
        else:
            freq[entry[i]] = 1

    for freq in freq.values():
        dataEntropy += (-freq/len(data))*math.log(freq/len(data), 2)

    return dataEntropy


def info_gain(attributes, data, attr ):
    freq = {}
    subsetEntropy = 0
    i = attributes.index(attr)
    for entry in data:
        if entry[i] in freq:
            freq[entry[i]] += 1.0
        else:
            freq[entry[i]] = 1.0
    for val in freq.keys():
        valProb = freq[val]/len(data)
        dataSubset = [entry for entry in data if entry[i] == val]
        subsetEntropy += valProb*entropy(attributes, dataSubset)

    return (entropy(attributes, data )-subsetEntropy)


def attr_choose(data, attributes):
    best = attributes[0]
    maxGain = 0
    for attr in attributes:
        newGain = info_gain(attributes, data, attr)
        if newGain > maxGain:
            maxGain = newGain
            best = attr
    return best

def get_data(data, attributes, best, val):
    new_data = [[]]
    index = attributes.index(best)
    for entry in data:
        if(entry[index] == val):
            newEntry = []
            for i in range(0, len(entry)):
                if(i != index):
                    newEntry.append(entry[i])
            new_data.append(newEntry)
    new_data.remove([])
    return new_data


def build_tree( data, attributes ):

    vals = [ record[-1] for record in data ]
    target = attributes[-1]

    # if data is empty or there is only one attributes 
    if not data or len((attributes)) == 1:
        default = majorClass( data )
        return default

    # if vals has only one type of value (yes/no). set() removes all duplicates
    elif len(set(vals)) == 1:
        return vals[0]

    else:
        best = attr_choose(data, attributes)
        tree = {best: {}}

        # get all possible values for best attriibute from dataset
        values = set ( [ row[attributes.index(best)] for row in data ] )
        
        for val in values: 
            new_data = get_data(data, attributes, best, val)
            newAttr = attributes[:]
            newAttr.remove(best)
            subtree = build_tree(new_data, newAttr )
            tree[best][val] = subtree
    return tree


def execute_decision_tree():

    with open("weather.csv") as tsv:
        data = list(csv.reader(tsv))
        print("Number of records:", len(data))

    training_set = data[1:]
    attributes = data[0]
    tree = build_tree( training_set, attributes )

    print(json.dumps(tree, indent=2))

    results = []
    test_set = [('rainy', 'mild', 'high', 'strong')]

    for entry in test_set:
        while(isinstance(tree, dict)):
            nodeVal = next(iter(tree))
            # child = tree[next(iter(tree))].keys()
            tree = tree[next(iter(tree))]
            index = attributes.index(nodeVal)
            value = entry[index]
            if (value in tree.keys()):
                result = tree[value]
                tree = tree[value]
        if result:
            results.append(result == entry[-1])
        print("Play : " + result)


if __name__ == "__main__":
    execute_decision_tree()
